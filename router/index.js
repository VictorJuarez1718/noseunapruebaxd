const express = require("express");
const router = express.Router();
const bodyParse = require("body-parser");



router.get('/', (req, res) => {
    const resultados = {

        nombre:req.query.nombre,
        edad:req.query.edad,
        cantidadProductos:req.query.cantidadProductos,
        precio:req.query.precio,
        totalAPagar:req.query.totalAPagar,
        subtotal:req.query.subtotal,
        descuento:req.query.descuento
       
    }
    
    res.render('index.html', resultados);
  });

router.post('/', (req, res)=>{
    const resultados = {
        nombre:req.body.nombre,
        edad:req.body.edad,
        cantidadProductos:req.body.cantidadProductos,
        precio:req.body.precio,
        totalAPagar:req.body.totalAPagar,
        subtotal:req.body.subtotal,
        descuento:req.body.descuento
        
    }
    res.render('index.html', resultados);
})
  





module.exports = router;